/*
Programa de inicio del servidor del API de service log

/********************************
 *      Adrián Suárez Parra     *
 *     Solusoft S.L 07/02/2017  *  
 *******************************

history:
    ASP 07022017 Crea

*/

//requires
var Logger = require('./libs/logger');
var transaction = require("./libs/middleware/transaction");
var error = require("./libs/middleware/error");
var metric = require("./libs/middleware/metric");
module.exports = {
    Logger: Logger,
    transaction: transaction,
    error: error,
    metric: metric
}

