/********************************
*      Adrián Suárez Parra     *
*     Solusoft S.L 02/03/2017  *  
*******************************

history:
   ASP 02032017 Crea

*/

var bunyan      = require('bunyan');
var unirest = require('unirest');
var config = require('../config.json');
module.exports = (function () {
    function BunyanServiceLog(uri, name) {
        config.service_log.uri = uri;
        config.service_log.providerId = name;
    }

    BunyanServiceLog.prototype.write = function write(rec) {
        var conf_sl = config.service_log;
        var route = conf_sl.uri + "/" + conf_sl.providerId + "/" + conf_sl.category;
        var log = buildLog(rec)
        unirest.post(route)
            .headers({ 'Content-Type': 'application/json' })
            .send(log)
            .end(function (response) {
                //console.log("Log sent: " + (response.code == 202 ? "ok" : JSON.stringify(response.body)));
            });
    };

    function buildLog(rec) {
        var now = rec.timestamp != null ? rec.timestamp : rec.time;
        var message = rec.message != null ? rec.message : rec.msg;
        var level = rec.level != null ?  mapLevelToName(rec.level) : "info";
        var log_built = {
            timestamp : now,
            log : {
                level : level,
                message : message,
                timestamp : now
            }
        }

        if (rec.reqId != null)
            log_built["reqId"] = rec.reqId;
        if (rec.geo != null)
            log_built["geo"] = rec.geo;
        if (rec.shape != null)
            log_built["shape"] = rec.shape;
        if (rec.domain != null)
            log_built["log"]["domain"] = rec.domain;
        if (rec.reason != null)
            log_built["log"]["reason"] = rec.reason;
        if (rec.code != null)
            log_built["log"]["code"] = rec.code;
        if (rec.payload != null)
            log_built["log"]["payload"] = rec.payload;

        return log_built;
    }

    function mapLevelToName(level) {
        var res = '';
        switch (level) {
            case bunyan.TRACE:
                res = 'trace';
                break;
            case bunyan.DEBUG:
                res = 'debug';
                break;
            case bunyan.INFO:
                res = 'info';
                break;
            case bunyan.WARN:
                res = 'warn';
                break;
            case bunyan.ERROR:
                res = 'error';
                break;
            case bunyan.FATAL:
                res = 'fatal';
                break;
        }
        return res;
    };

    return BunyanServiceLog;
})();