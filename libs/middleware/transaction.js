/*
Middleware que gestiona la transacción dentro de la que se ejecuta una operación

history: 
    MJGS 16022016 Crea
*/
var uuid = require('node-uuid');

/**
 * Prepara el objeto transacción que acompañará a la operación desde su creación hasta la respuesta
 */
var makeTranReady = function (req, res, next) {
  let requestedReqId = null;
  if (req && req.headers) requestedReqId = req.headers["x-request-id"]; //lowercased by framework

  if (res.locals == null) {
    res.locals = {};
  }
  res.locals.reqId = requestedReqId || uuid.v4(),  //almacena el identificador de la peticion, se utiliza para tracear las operaciones
  res.locals.error = null, // { code: int, message : string }
  res.locals.errors = [] // optional. almacena los errores internos detectados, se utiliza para devolver errores enriquecidos { domain : string, reason: string, message: string, extendedHelp: string }

  next();
}

module.exports = makeTranReady;