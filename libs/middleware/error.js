/*
Middleware que gestiona las excepciones
doc: http://expressjs.com/es/guide/error-handling.html

history: 
    MJGS 16022016 Crea
*/
var uuid = require('node-uuid');
var logger = require("../logger");

/**
 * Obtiene el objeto JSON estandar de respuesta a los mensajes de error
 */
function getErrorJsonResponse(code, message, extraErrors) {
    var response = {
        error: {
            code: code,
            message: message
        }
    };

    if (extraErrors && extraErrors.length > 0) {
        response.error.errors = extraErrors;
    }

    return response;
}

/**
 * Prepara el objeto transacción que acompañará a la operación desde su creación hasta la respuesta
 */
var manageExceptions = function (err, req, res, next) {

    var status, message;


    if (err && err.code && err.message) {
        status = err.code;
        message = '[Request ' + res.locals.reqId + '] ' + err.message;
        req.app.log.addResponseError({ level: "error", message: err.message, code: message.code, res: res });
    } else if (res.locals.error) {
        status = res.locals.error.code;
        message = '[Request ' + res.locals.reqId + '] ' + res.locals.error.message;
    } else {
        status = 500;
        message = '[Request ' + res.locals.reqId + '] ' + 'Unexpected Internal Error';
        error = {
            "code": 500,
            "message": err.stack
        }
        req.app.log.addResponseError({ level: "fatal", message: err.message, code: message.code, res: res });
    }

    res.status(status);
    res.set({ "X-Request-Id": res.locals.reqId });
    res.json(getErrorJsonResponse(status, message, res.locals.errors));

    res.send();
};

module.exports = manageExceptions;
