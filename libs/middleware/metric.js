var uuid = require('node-uuid');
var unirest = require('unirest');
var config = require('../../config.json');


/**
 * Prepara el objeto transacción que acompañará a la operación desde su creación hasta la respuesta
 */
var sendMetrics = function (req, res, next) {


    if (req.app.log.streamExists("sl")) {

        var info_req = getRequestInfo(req);

        if (res.locals.reqId == null) {
            res.locals = {
                reqId: uuid.v4(),          //almacena el identificador de la peticion, se utiliza para tracear las operaciones
                error: null,                    // { code: int, message : string }
                errors: []                      // optional. almacena los errores internos detectados, se utiliza para devolver errores enriquecidos { domain : string, reason: string, message: string, extendedHelp: string }
            };
        }
        
        var metric = buildInitMetric(res);
        var conf_sl = req.app.log.getStreamInfo("sl");
        var route = conf_sl.uri + "/" + conf_sl.domain_name + "/metrics";
        res.locals.route = route;
        res.locals.location = metric["metricId"]
        unirest.post(route)
            .headers({ 'Content-Type': 'application/json' })
            .send(metric)
            .end(function (response) {
            });

        res.on('finish', function () {
            var metric = buildEndMetric(req, res);
            updateMetric(metric, res);
        });

        next();
    } else {
        next();
    }

}


function getRequestInfo(req) {
    var info = {
        headers: req.headers,
        uri: req.url,
        body: req.body,
        method: req.method
    }
    return info;
}

function updateMetric(metric, res) {
    route = res.locals.route + "/" + res.locals.location;
    unirest.put(route)
        .headers({ 'Content-Type': 'application/json' })
        .send(metric)
        .end(function (response) {
        });

}

function buildInitMetric(res) {

    var now = new Date();
    var metric = {
        "reqId": res.locals.reqId,
        "metricId": uuid.v4(),
        "timestamp": now,
        "metrics": {
            "from": now,
            "value": {}
        }
    }
    return metric;
}


function buildEndMetric(req, res) {
    var uri = req.protocol + '://' + req.get('host') + req.originalUrl;
    var body = JSON.stringify(req.body);
    var headers = req.headers;
    var verb = req.method;
    var now = new Date();
    var metric = {
        "reqId": res.locals.reqId,
        "metrics": {
            "to": now,
            "value": {
                "uri": uri,
                "body": body,
                "headers": headers,
                "verb": verb
            }
        }
    }
    return metric;
}
module.exports = sendMetrics;