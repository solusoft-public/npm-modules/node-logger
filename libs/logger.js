/*
Wrapper para el mecanismo de Log

history: 
    MJGS 17022016 Crea, basado en bunyan
*/
var bunyan = require('bunyan'),
    BunyanMongo = require('./bunyan-mongo'),
    BunyanServiceLog = require('./bunyan-service-log');

/**
 * Wrapper
 * 
 * @param options - opciones de inicializacion  
 * { 
    name: "nombre del logger", 
    streams: [
        {
            level: 'trace',
            stream: 'stdout'
        },{
            path: '/var/log/foo.log',
            type: 'file',
            level: 'info'
        }] }
 */
var Logger = function Logger(options) {

    var streams = compatStreams(options.streams);

    var logger = bunyan.createLogger({ name: options.name, streams: streams });

    /**
     * Valida el valor del parametro level
     */
    function getValidLevel(level) {
        switch (level) {
            case 'trace': return level;
            case 'debug': return level;
            case 'info': return level;
            case 'warn': return level;
            case 'error': return level;
            case 'fatal': return level;
            default:
                logger.warn('Invalid log level value: ' + level);
                return 'warn';
        }
    }

  
   var streamExists = function streamExists(streamName){
       var stream;
       var i = 0;
       var found = false;
       while( i < options.streams.length && found == false) {
            stream = options.streams[i];
            if(stream.stream == streamName)
               found = true;
            i++;
       }
       return found;
   }

   var getStreamInfo = function getStreamInfo(streamName){
       var info = {};

       for (var i = 0; i < options.streams.length; i++) {
           if(options.streams[i].stream == streamName){
               info = options.streams[i];
           }
       }
       info["domain_name"] = options.name;
       
       return info;
   }

    /**
     * Añade un error que llegará al usuario 
     * @param {String} level - nivel del error "trace", "info", "debug", "error", "fatal", "warn"
     * @param {String} message mensaje de traza que se registrara
     * @param {Integer} code, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} res - referencia a la response donde se utilizara locals.error y locals.errors para almacenar los errores
     */
    var addResponseError = function addResponseError({ level, message, code, res, domain = null, reason = null }) {

      const error = {
        message: message,
        code: code,
      };

      if (domain !== null) error.domain = domain;
      if (reason !== null) error.reason = reason;

        if (res){
            if(res.locals.error){
                res.locals.errors.push(res.locals.error)
                res.locals.error = error
            }
            else {
                res.locals.error = error
            }
        }
        logger[getValidLevel(level)]({ reqId: res.locals ? res.locals.reqId : null, code: error.code || null }, error.message);
    };

    /**
     * Logging de librerias externas que utilice tu aplicacion o algo muy detallado de tu propia aplicación
     * @param {String} message mensaje de traza que se registrara
     * @param {String} reqId Opcional, identificador que permite la correlación de las trazas
     * @param {Integer} code Opcional, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} payload Opcional, objeto serializable que contiene información asociada al error
     * @param {Object} geo Opcional, objeto GEOJson con la posicion del log
     * @param {Object} shape Opcional, objeto GEOJson con la figura del log
     * @param {timestamp} timestamp Opcional, fecha en formato ISO 8601
     */
    var trace = function trace({ message = null, reqId = null, code = null, domain = null, reason = null, payload = null, geo = null, shape = null, timestamp = null }) { 
      return logger.trace({ reqId: reqId, code:code, domain:domain, reason:reason, payload: payload, geo: geo, shape: shape, timestamp }, message); 
    };

    /**
     * Cualquier cosa, demasiado "verbose" para incluirse en el nivel "info"
     * @param {String} message mensaje de traza que se registrara
     * @param {String} reqId Opcional, identificador que permite la correlación de las trazas
     * @param {Integer} code Opcional, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} payload Opcional, objeto serializable que contiene información asociada al error
     * @param {Object} geo Opcional, objeto GEOJson con la posicion del log
     * @param {Object} shape Opcional, objeto GEOJson con la figura del log
     * @param {timestamp} timestamp Opcional, fecha en formato ISO 8601* 
     */
    var debug = function debug({ message = null, reqId = null, code = null, domain = null, reason = null, payload = null, geo = null, shape = null, timestamp = null }) { 
      return logger.debug({ reqId: reqId, code:code, domain:domain, reason:reason, payload: payload, geo: geo, shape: shape, timestamp }, message); 
    };

    /**
     * Detail on regular operation.
     * @param {String} message mensaje de traza que se registrara
     * @param {String} reqId Opcional, identificador que permite la correlación de las trazas
     * @param {Integer} code Opcional, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} payload Opcional, objeto serializable que contiene información asociada al error
     * @param {Object} geo Opcional, objeto GEOJson con la posicion del log
     * @param {Object} shape Opcional, objeto GEOJson con la figura del log
     * @param {timestamp} timestamp Opcional, fecha en formato ISO 8601
     */
    var info = function info({ message = null, reqId = null, code = null, domain = null, reason = null, payload = null, geo = null, shape = null, timestamp = null }) { 
      return logger.info({ reqId: reqId, code:code, domain:domain, reason:reason, payload: payload, geo: geo, shape: shape, timestamp }, message); 
    };

    /**
     * Una anotación de algo que deberia ser probablemente observado y tenido en cuenta por un operador de la aplicación en algun momento.
     * @param {String} message mensaje de traza que se registrara
     * @param {String} reqId Opcional, identificador que permite la correlación de las trazas
     * @param {Integer} code Opcional, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} payload Opcional, objeto serializable que contiene información asociada al error
     * @param {Object} geo Opcional, objeto GEOJson con la posicion del log
     * @param {Object} shape Opcional, objeto GEOJson con la figura del log
     * @param {timestamp} timestamp Opcional, fecha en formato ISO 8601
     */
    var warn = function warn({ message = null, reqId = null, code = null, domain = null, reason = null, payload = null, geo = null, shape = null, timestamp = null }) { 
      return logger.warn({ reqId: reqId, code:code, domain:domain, reason:reason, payload: payload, geo: geo, shape: shape, timestamp }, message); 
    };

    /**
     * Un error fatal para una petición o operación específica, que no impida que la aplicación o servicio continue sirviendo otras peticiones correctamente. 
     * Un operador deberia analizar esto pronto.
     * @param {String} message mensaje de traza que se registrara
     * @param {String} reqId Opcional, identificador que permite la correlación de las trazas
     * @param {Integer} code Opcional, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} payload Opcional, objeto serializable que contiene información asociada al error
     * @param {Object} geo Opcional, objeto GEOJson con la posicion del log
     * @param {Object} shape Opcional, objeto GEOJson con la figura del log
     * @param {timestamp} timestamp Opcional, fecha en formato ISO 8601     
     */
    var error = function error({ message = null, reqId = null, code = null, domain = null, reason = null, payload = null, geo = null, shape = null, timestamp = null }) { 
      return logger.error({ reqId: reqId, code:code, domain:domain, reason:reason, payload: payload, geo: geo, shape: shape, timestamp }, message); 
    }

    /**
     * El servicio o aplicación se ha detenido o va a quedar inusable ahora. Un operador deberia mirar esto urgentemente.
     * @param {String} message mensaje de traza que se registrara
     * @param {String} reqId Opcional, identificador que permite la correlación de las trazas
     * @param {Integer} code Opcional, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} payload Opcional, objeto serializable que contiene información asociada al error
     * @param {Object} geo Opcional, objeto GEOJson con la posicion del log
     * @param {Object} shape Opcional, objeto GEOJson con la figura del log
     * @param {timestamp} timestamp Opcional, fecha en formato ISO 8601
     */
    var fatal = function fatal({ message = null, reqId = null, code = null, domain = null, reason = null, payload = null, geo = null, shape = null, timestamp = null }) { 
      return logger.fatal({ reqId: reqId, code:code, domain:domain, reason:reason, payload: payload, geo: geo, shape: shape, timestamp }, message); 
    }
    

    return {
        trace: trace,
        debug: debug,
        info: info,
        warn: warn,
        error: error,
        fatal: fatal,
        addResponseError: addResponseError,
        streamExists : streamExists,
        getStreamInfo : getStreamInfo
  };

    /**
    * Compatibiliza los streams de entrada (opciones) con los aceptados por la implementación de bunyan
    */
    function compatStreams(streams) {

        var output = [];

        if (streams) {
            var aux;
            for (var i = 0; i < streams.length; i++) {
                aux = streams[i];
                if (streams[i].stream == 'stdout') {
                    output.push({ level: aux.level, stream: process.stdout });
                }
                if (streams[i].type == 'file') {
                    var path = process.cwd() + streams[i].path;
                    output.push({ type: aux.type, level: aux.level, path: path });
                }
                if (streams[i].stream == 'mongodb') {
                    var stream = new BunyanMongo(streams[i]["uri"], options.name);
                    output.push({ type: aux.type, level: aux.level, stream: stream });
                }

                if (streams[i].stream == 'sl') {
                    var stream = new BunyanServiceLog(streams[i]["uri"], options.name);
                    output.push({ type: aux.type, level: aux.level, stream: stream });
                }
            }
        }
        return output;
    }
};

module.exports = Logger;