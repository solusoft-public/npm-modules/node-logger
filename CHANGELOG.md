# Node-logger

# 2.1.1

* Acepta el parámetro timestamp  

# 2.1.0

* Acepta los parametros geo y shape de la nueva versión de service-logs
* Eliminada traza de log olvidada y que provocaba errores

# 2.0.0

* Si existe el header X-Request-Id, lo utilizará como reqId en lugar de generar uno nuevo.
* Ahora la invocación a los métodos se hace con parametros desestructurados, para facilitar la invocación y la lectura del código al haber muchos parametros opcionales

# 1.1.1

* Acepta el parametro payload

# 1.1.0

* Modificada funcionalidad del middleware de métricas para hacer asíncrona su funcionalidad.
* Corregidos pequeños errores de implementación.

