# Node-logger
=========================

Microservicio que registra trazas usando Bunyan en MongoDB y Service logs.

=========================

## Source code

La implementación se realiza en NodeJS.
El código fuente se encuentra en la carpeta `src`.

## Puesta en marcha

Requiere un entorno de ElasticSearch al que conectarse.

### Instalación como paquete externo


* Abra una CMD de Windows o Shell de Ubuntu. Es importante que no sea Git Bash.
* Ubiquese en la carpeta de su proyecto donde se encuentre el package.json

Ejecute lo siguiente:

```sh
$ npm install --save git+https://gitlab.com/solusoft-public/npm-modules/node-logger.git#2.1.0
```

### Uso

#### Instancia de node-logger
```sh
	var nl = require('node-logger');
```    
#### Logger

##### Definicion

Funcionalidad que registra trazas en el sistema

##### Constructor

Hay que pasarle una configuración para darle datos como donde está ubicada tu base de datos Mongo, donde tienes ubicado el service-log...
```sh
		var logger = nl.Logger(config);
```

Ejemplo de config.json:

	{
    "log": {
        "name": "bunyan-traces",
        "streams": [
            {
                "level": "trace",
                "stream": "stdout"
            },
            {
                "level": "trace",
                "type": "raw",
                "stream": "sl",
                "uri": "http://localhost:10030/logs/v0/"
            },
            {
                "level": "trace",
                "type": "raw",
                "stream": "mongodb",
                "uri": "mongodb://localhost/fs-log"
            },
            {
            	"type": "file",
                "level": "info",
                "path": "/logs/api.log"
            }
        ]
    }
}

__Aviso__ : _No es necesario añadir todos los streams, puedes añadir únicamente los que sean necesarios_

##### Métodos

* **fatal**: Sirve para generar un log que tendrá nivel *fatal*. Este error se usa cuando el servicio o aplicación se ha detenido o ha llegado a ser inutilizable y se ha roto.

* **error**: Sirve para generar un log que tendrá nivel *error*. Indica que hubo un error en un/a método/petición de la aplicación pero el servicio/app puede continuar con su funcionamiento recibiendo otras peticiones.

* **warn**: Sirve para generar un log que tendrá nivel *warn*. Es un aviso de que es probable que la parte señalada deba ser revisada.

* **info**: Sirve para generar un log que tendrá nivel *info*. Es para expresar detalles sobre una operación determinada.

* **debug**: Sirve para generar un log que tendrá nivel *debug*. Es cualquier otra cosa, es decir, es incluso demasiado detallado para ser *info*

* **trace**: Sirve para generar un log que tendrá nivel *trace*. Registro de bibliotecas externas usadas por su aplicación o registro de aplicaciones muy detallado.

##### Parámetros

Todos los métodos pueden recibir los siguientes parámetros:

* **message**: <span style="color:red">**(Obligatorio)**</span>
	*  **Tipo**: String
	*  **Descripción**: Es el mensaje que tendrá su traza.
	*  **Ejemplo**: "Todo fue un éxito!"

* **reqId**: <span style="color:green">**(Opcional)**</span>
	* **Tipo**: String
	* **Descripcion**: Es el identificador de su traza. Si no lo especifica se autogenera.
	* **Ejemplo**: "3refefg455r"

* **code**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : Number
	* **Descripción**: Es el código de la respuesta a una petición
	* **Ejemplo** : 200  *(significa OK)*

* **domain**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : String
	* **Descripción**: Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que hizo la petición,...
	* **Ejemplo** : "proveedor1"


* **reason**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : String
	* **Descripción**: Es la razón por la que se produjo la traza.
	* **Ejemplo** : "I'm good programmer!"

* **payload**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : Object
	* **Descripción**: Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza.
	* **Ejemplo** : { "cpu": 30, "ram": "1Gb" }

* **geo**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : GeoJson Object
	* **Descripción**: El geoposicionamiento para esta traza. Longitud-latitud.
	* **Ejemplo** : { "type" : "point", "coordinates" : [72.57, 23.02] }

* **shape**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : GeoJson Object
	* **Descripción**: Una figura o forma que puede ser agregada al log para analisis geoespacial. __OJO: Aun en preview, el rendimiento puede empeorar__
	* **Ejemplo** : { "type" : "linestring", "coordinates" : [[77.57, 23.02], [77.59, 23.05]] }

* **timestamp**: <span style="color:green">**(Opcional)**</span>
	* **Tipo** : String
	* **Descripción**: Fecha en la que se produjo la traza. Si no se especifica se coge la traza actual.  
	* **Ejemplo** : "2019-09-23T02:55:43Z"

Los métodos tendrán la forma *log.metodo(message, reqId, code, domain, reason, payload)*
##### Ejemplos de uso

* logger.info({ message: "Connected!" })
* logger.fatal({ message: "Crash all!!", reason: "I'm bad programmer!" })
* logger.debug({ message: "I don't know why do this", reqId: "gv5rtbth4gh45", domain: "modulo1" })
* logger.trace({ message: "Passed by here", geo" : { "type" : "point", "coordinates" : [72.57, 23.02] } })
* logger.error({ message: "Bad request", code: 400, reason: "my request is bad" })
* logger.warn({ message: "This method crash sometimes", domain: "domain\_of\_method", reason: "Somebody should check this" })


#### Middleware transaction y Middleware error

##### Definicion

Middleware propios de node-logger. El middleware de transaction se encarga de inicializar las variables necesarias para el uso posterior del middleware de error. El middleware de error, devuelve todos los errores registrados con la función __addResponseError()__ de __Logger__ y actúa a su vez como middleware de error del sistema, gestionando tanto los errores que comete el desarrollador al desarrollar su api (registrando _stack_ del error en su conjunto de streams y devolviendo el mensaje al usuario) como los errores cometidos por el usuario al utilizarla (registrando el mensaje de error y devolviéndolo al usuario).

##### Constructor

A continuación se describe como debería ser la estructura del _main_ del API utilizando estos middleware:

```sh
var tranMiddleware = nl.transaction;
var errorMiddleware = nl.error;
var router = require('./router');
...
app.use(tranMiddleware);
...
app.use('/prueba', api);
...
app.use(errorMiddleware);
```

##### Ejemplos de uso

Para poder aprovechar la funcionalidad de nuestro middleware, se registraran errores que se quieran registrar en su conjunto de streams y a su vez enviarse al usuario que ha hecho la petición.

A continuación un ejemplo:
```sh
function searchUsers(req, res, next) {

    var u = db.find();

    logger.trace("se obtuvieron " + u.count + "users"); //POST

    if (u.count == 0) {
    var err = {
    	"code" : 404,
        "message" : "not users found"
    }
                   logger.addResponseError({ level: "warn", code: 404, message: "not users found", res: res);
    }              
}

```


Cuando haya un error en el API, se enviarán todos los errores registrados en addResponseError mediante el middleware de error. Tendrá la siguiente estructura:

```sh
{
  "error": {
    "code": 500,
    "message": "[Request 451b957d-4a84-4261-8727-ce8669ba0b55] Unexpected Internal Error",
    "errors": [
        {
            "code" : 404,
            "message" : "not users found"
        }
    ]
  }
}
```

Siendo el error principal, el que ha generado el fallo en su API.



#### Middleware metrics

##### Definicion
El módulo metrics utiliza la configuración pasada como parámetro a __Logger__ para capturar el inicio y el fin de de las peticiones que recibe el API en el que se implante. Es decir, guardará métricas de la ejecución de la petición en el API en service-log.

##### Constructor

```sh
var metricMiddleware = nl.metric;
...
app.use(metricMiddleware);
...

```

__AVISO__ : Recordar que para capturar correctamente la métrica, el middleware de métricas debe declararse como el primer middleware por el que pasará la petición que reciba el API. Y previamente es __necesario__ a ver inicializado el __Logger__ de __node-logger__ con la configuración pertinente.


### Configuración

El fichero `src/config.json` contiene toda la configuración  del módulo.
En este caso, contiene las rutas tanto de la ubicación del service log como de mongo y los logs para inicializar logger

## Contributing & License

Solusoft  



Todos los derechos reservados. Consultar [www.solusoft.es](http://www.solusoft.es/contactar.aspx)
